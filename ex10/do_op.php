#!/usr/bin/php
<?php
	if ($argc != 4)
	{
		echo "Incorrect Parameters\n";
		exit ;
	}
	$num1 = (int)trim($argv[1]);
	$operator = trim($argv[2]);
	$num2 = (int)trim($argv[3]);
	switch ($operator)
	{
		case "+":
			$res = $num1 + $num2;
			break;
		case "-":
			$res = $num1 - $num2;
			break;
		case "*":
			$res = $num1 * $num2;
			break;
		case "/":
			$res = $num1 / $num2;
			break;
		case "%":
			$res = $num1 % $num2;
			break;
	}
	echo $res."\n";
?>
