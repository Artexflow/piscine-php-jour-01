#!/usr/bin/php
<?php
	print("Entrez un nombre: ");
	while (($line = fgets(STDIN)) != FALSE)
	{
		$line = trim($line);
		if (is_numeric($line))
		{
			$c = substr($line, -1);
			if (intval($c) % 2 == 0)
				printf("Le chiffre $line est Pair\n");
			else
				printf("Le chiffre $line est Impair\n");
		}
		else
			printf("'$line' n'est pas un chiffre\n");
	print("Entrez un nombre: ");
	}
?>
