#!/usr/bin/php
<?php
	if ($argc < 2)
		exit ;
	$array = array();
	$i = 0;
	$to_find = $argv[1];
	foreach ($argv as $val)
	{
		if (($i == 0 && ++$i) || ($i == 1 && ++$i))
			continue ;
		$key = preg_split('/:/', $val);
		$avalue = strstr($val, ":");
		$array[$key[0]] = substr($avalue, 1);
	}
	if (array_key_exists($to_find, $array))
	{
		if (isset($array[$to_find]))
			echo $array[$to_find]."\n";
		else
			echo "\n";
	}
?>
