#!/usr/bin/php
<?php

	function	is_letter($c)
	{
		if (($c >= 65 && $c <= 90) || ($c >= 97 && $c <= 122))
			return TRUE;
		else
			return FALSE;
	}

	function	is_num($c)
	{
		if ($c >= 48 && $c <= 57)
			return TRUE;
		else
			return FALSE;
	}

	function	asciival($str, $i)
	{
		return (ord(substr($str, $i, 1)));
	}

	function	case_cmp($c1, $c2)
	{
		if ($c1 >= 97 && $c1 <= 122)
			$cmp1 = $c1 - 32;
		else
			$cmp1 = $c1;
		if ($c2 >= 97 && $c2 <= 122)
			$cmp2 = $c2 - 32;
		else
			$cmp2 = $c2;
		return ($cmp1 > $cmp2 ? 1 : -1);
	}

	function	cmp_function($str1, $str2)
	{
		if (strcasecmp($str1, $str2) == 0)
			return (0);
		$i = 0;
		$str1_len = strlen($str1);
		$str2_len = strlen($str2);
		while ($i < $str1_len && $i < $str2_len && asciival($str1, $i) == asciival($str2, $i))
			++$i;
		$c1 = ord(substr($str1, $i, 1));
		$c2 = ord(substr($str2, $i, 1));
		if ($i == $str1_len)
			return (-1);
		else if ($i == $str2_len)
			return (1);
		else if (is_letter($c1))
		{
			if (is_num($c2))
				return (-1);
			else if (is_letter($c2))
				return (case_cmp($c1, $c2));
			else
				return (-1);
		}
		else if (is_num($c1))
		{
			if (is_num($c2))
			{
				if ($c1 > $c2)
					return (1);
				else
					return (-1);
			}
			else if (is_letter($c2))
				return (1);
			else
				return (-1);
		}
		else
		{
			if (is_num($c2))
				return (1);
			else if (is_letter($c2))
				return (1);
			else
			{
				if ($c1 > $c2)
					return (1);
				else
					return (-1);
			}
		}
	}

	if ($argc < 2)
		exit ;
	$to_print = array();
	foreach ($argv as $i => $val)
	{
		if ($i == 0)
			continue ;
		else
			$to_print = array_merge($to_print, preg_split('/\s+/', trim($argv[$i])));
	}
	usort($to_print, "cmp_function");
	foreach ($to_print as $value)
		echo $value."\n";
?>
