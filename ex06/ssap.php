#!/usr/bin/php
<?php
	if ($argc < 2)
		exit ;
	$to_print = array();
	foreach ($argv as $i => $val)
	{
		if ($i == 0)
			continue ;
		else
			$to_print = array_merge($to_print, preg_split('/\s+/', trim($argv[$i])));
	}
	sort($to_print, SORT_STRING);
	foreach ($to_print as $word)
		echo $word."\n";
?>
