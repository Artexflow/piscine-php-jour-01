#!/usr/bin/php
<?php

	function	moyenne()
	{
		$qty = 0;
		$sum = 0;
		while (($line = fgets(STDIN)) != FALSE)
		{
			if ($i++ == 0)
				continue ;
			else
			{
				$tmp = explode(";", $line);
				if ($tmp[2] !== "moulinette" && $tmp[1] !== "")
				{
					$sum += intval($tmp[1]);
					++$qty;
				}
			}
		}
		echo $sum / $qty."\n";
	}

	function	moyenne_user()
	{
		$data = array();
		$i = 0;

		while (($line = fgets(STDIN)) != FALSE)
		{
			if ($i++ == 0)
				continue ;
			$tmp = explode(";", $line);
			if ($tmp[2] !== "moulinette" && $tmp[1] !== "")
				$data[$tmp[0]]['note'][] = $tmp[1];
			else if ($tmp[2] === "moulinette" && $tmp[1] !== "")
				$data[$tmp[0]]['moulinette'][] = $tmp[1];
		}
		ksort($data);
		foreach ($data as $user_name => $usr_data)
		{
			$qty = 0;
			$sum = 0;
			foreach ($usr_data['note'] as $note)
			{
				++$qty;
				$sum += $note;
			}
			echo $user_name.":".$sum / $qty."\n";
		}
	}

	function	ecart_moulinette()
	{
		$data = array();
		$i = 0;

		while (($line = fgets(STDIN)) != FALSE)
		{
			if ($i++ == 0)
				continue ;
			$tmp = explode(";", $line);
			if ($tmp[2] !== "moulinette" && $tmp[1] !== "")
				$data[$tmp[0]]['note'][] = $tmp[1];
			else if ($tmp[2] === "moulinette" && $tmp[1] !== "")
				$data[$tmp[0]]['moulinette'] = $tmp[1];
		}
		ksort($data);
		foreach ($data as $user_name => $usr_data)
		{
			$sum = 0;
			foreach ($usr_data['note'] as $note)
			{
				$sum += $note - $usr_data['moulinette'];
				++$qty;
			}
			echo $user_name.":".$sum / $qty."\n";
		}
	}

	if ($argc != 2 || ($argv[1] !== "moyenne" && $argv[1] !== "moyenne_user" && $argv[1] !== "ecart_moulinette"))
		exit ;
	$i = 0;
	if ($argv[1] === "moyenne")
		moyenne();
	else if ($argv[1] === "moyenne_user")
		moyenne_user();
	else if ($argv[1] === "ecart_moulinette")
		ecart_moulinette();
?>
