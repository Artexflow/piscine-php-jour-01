#!/usr/bin/php
<?php

function	is_operator($str)
{
	if ($str === "+" || $str === "-" || $str === "*" || $str === "/" || $str ==="%")
		return TRUE;
	else
		return FALSE;
}

function	is_valid($params, $params_size)
{
	if ($params_size == 3 && is_numeric($params[0]) && is_operator($params[1]) && is_numeric($params[2]))
		return array(TRUE, array(intval($params[0]), $params[1], intval($params[2])));
	else if ($params_size == 4
		&& ($params[0] === "+" || $params[0] === "-")
		&& is_numeric($params[1])
		&& is_operator($params[2])
		&& is_numeric($params[3]))
		return array(TRUE, array($params[0] === "+" ? intval($params[1]) : intval($params[1]) * -1, $params[2], intval($params[3])));
	else if ($params_size == 4
		&& is_numeric($params[0])
		&& is_operator($params[1])
		&& ($params[2] === "+" || $params[2] === "-")
		&& is_numeric($params[3]))
		return array(TRUE, array(intval($params[0]), $params[1], $params[2] === "+" ? intval($params[3]) : intval($params[3]) * -1));
	else if ($params_size == 5
		&& ($params[0] === "+" || $params[0] === "-")
		&& is_numeric($params[1])
		&& is_operator($params[2])
		&& ($params[3] === "+" || $params[3] === "-")
		&& is_numeric($params[4]))
		return array(TRUE, array($params[0] === "+" ? intval($params[1]) : intval($params[1]) * -1,
			$params[2],
			$params[3] === "+" ? intval($params[4]) : intval($params[4]) * -1));
	else
		return array(FALSE, NULL);
}

function specified_array_unique($array, $value) 
{ 
	$count = 0; 
	$number = 0;

	foreach($array as $array_key => $array_value) 
	{ 
		if (($count > 0) && ($array_value == $value) && ($value === "+" || ($value === "-" && ($number == 0 || $count > 1))))
			unset($array[$array_key]); 
		if ($array_value == $value)
			$count++; 
		if (is_numeric($array_value) && ++$number)
			$count = 0;
	} 
	return array_filter($array); 
} 

function	check_args($params)
{
	$params = array_values(specified_array_unique(array_values(specified_array_unique($params, "+")), "-"));
	$params_size = count($params);
	if (!($params_size >= 3 && $params_size <= 5))
		return (NULL);
	list($valid, $calculus) = is_valid($params, $params_size);
	if ($valid)
		return ($calculus);
	else
		return NULL;
}

function	do_the_math($num1, $operator, $num2)
{
	switch ($operator)
	{
	case "+":
		$res = $num1 + $num2;
		break;
	case "-":
		$res = $num1 - $num2;
		break;
	case "*":
		$res = $num1 * $num2;
		break;
	case "/":
		$res = $num1 / $num2;
		break;
	case "%":
		$res = $num1 % $num2;
		break;
	}
	echo $res."\n";
}

if ($argc != 2)
	exit ("Incorrect Parameters\n");
$pattern = array('/\*/', '/\+/', '/-/', '/\//', '/%/');
$replacement = array(" * ", " + ", " - ", " / ", " % ");
$params = preg_split('/\s+/', preg_replace($pattern, $replacement, trim($argv[1])));
if (($calculus = check_args($params)) == NULL)
	exit ("Syntax Error\n");
else
	do_the_math($calculus[0], $calculus[1], $calculus[2]);
?>
